import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DesktopComponent } from './desktop/desktop.component';
import { CatalogComponent } from './catalog/catalog.component';
import { ReportsComponent } from './reports/reports.component';
import { SalesComponent } from './sales/sales.component';
import { ProfilesListComponent } from './profiles/list/profiles-list.component';
import { ProfilesEditComponent } from './profiles/edit/profiles-edit.component';
import { SystemComponent } from './system.component';

const systemRoutes: Routes = [
  { path: 'system', component: SystemComponent, children: [
    { path: 'desktop', component: DesktopComponent },
    { path: 'catalog', component: CatalogComponent, children: [] },
    { path: 'profiles', component: ProfilesListComponent, children: [
        { path: 'edit/:id', component: ProfilesEditComponent },
        { path: 'create', component: ProfilesEditComponent }
    ]},
    { path: 'reports', component: ReportsComponent, children: [] },
    { path: 'sales', component: SalesComponent }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(systemRoutes)],
  exports: [RouterModule]
})
export class SystemRoutingModule {

}
