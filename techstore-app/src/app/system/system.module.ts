import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SystemRoutingModule } from './system-routing.module';
import { SystemComponent } from './system.component';
import { DesktopModule } from './desktop/desktop.module';
import { SalesModule } from './sales/sales.module';
import { ReportsModule } from './reports/reports.module';
import { ProfilesModule } from './profiles/profiles.module';
import { CatalogModule } from './catalog/catalog.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [SystemComponent],
  imports: [
    CatalogModule,
    CommonModule,
    DesktopModule,
    ProfilesModule,
    ReportsModule,
    SalesModule,
    SharedModule,
    SystemRoutingModule
  ]
})
export class SystemModule {

}
