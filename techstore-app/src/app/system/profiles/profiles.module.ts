import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProfilesEditComponent} from './edit/profiles-edit.component';
import {ProfilesListComponent} from './list/profiles-list.component';

@NgModule({
  declarations: [
    ProfilesListComponent,
    ProfilesEditComponent
  ],
  imports: [
    CommonModule
  ]
})

export class ProfilesModule {

}
