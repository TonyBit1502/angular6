import { Component, Input, OnInit } from '@angular/core';
import { NotifierService } from 'angular-notifier';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private readonly notifier: NotifierService;
  @Input() login: string;
  @Input() password: string;

  constructor(
    private router: Router,
    private notifierService: NotifierService
  ) {
    this.notifier = notifierService;
  }

  ngOnInit() {

  }

  apply() {
    if (!this.login || !this.password) {
      this.notifier.notify('error', 'Введите логин и пароль');
    } else {
      this.router.navigateByUrl('/system/desktop');
    }
  }

  signUp() {

  }
}
