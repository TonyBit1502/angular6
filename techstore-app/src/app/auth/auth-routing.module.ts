import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";

import { AuthComponent } from "./auth.component";
import { LoginComponent } from "./login/login.component";
import { SignUpComponent } from "./sign-up/sign-up.component";

const authRoutes : Routes = [
  { path: '', component: AuthComponent, children: [
    { path: 'login', component: LoginComponent},
    { path: 'sign_up', component: SignUpComponent }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(authRoutes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {

}
