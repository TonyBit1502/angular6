package com.techstore.springbootangular.controllers;

import com.techstore.springbootangular.pojo.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileController {
    public static final String PROFILE_HEADER = "Profile";

    @Autowired
    private ProfileRepository profileRepository;


    @RequestMapping(method = RequestMethod.GET)
    public List<Profile> get(@RequestHeader(value = PROFILE_ID_HEADER, required = false) Integer profileId,
                             @RequestHeader(value = PROFILE_HEADER) Profile profile,
                             @RequestParam(value = "ids", required = false) List<Integer> ids,
                             @RequestParam(value = "type", required = false) String type,
                             @RequestParam(value = "city", required = false) String city,
                             @RequestParam(value = "country", required = false) String country,
                             @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                             @RequestParam(value = "per_page", required = false, defaultValue = "50") Integer perPage,
                             HttpServletResponse response) {

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Profile getById(@RequestHeader(value = PROFILE_ID_HEADER, required = false) Integer profileId,
                           @RequestHeader(value = PROFILE_HEADER) Profile profile,
                           @PathVariable(value = "id") Integer id) {
        Map<String, Object> params = new HashMap<>();
    }
}
